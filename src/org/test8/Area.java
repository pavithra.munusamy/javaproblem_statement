package org.test8;

public class Area extends Shape {
	  

		double area;

		@Override
		void rectangle_Area(int length, int breadth) {
			area = length * breadth;
		       System.out.println("Area of rectangle is: " + area);
			
		}

		@Override
		void square_Area(int side) {
			area = side * side;
		       System.out.println("Area of Square is: " + area);
			
		}

		@Override
		void circle_Area(float radius) {
			area = (radius * radius) * 3.14;
		       System.out.println("Area of Circle is: " + area);
			
		}
		
		public static void main(String[] args) {
		    Area a=new Area();
		    a.rectangle_Area(30, 40);
		    a.square_Area(50);
		    a.circle_Area(60);
		    
		    
		}


}
