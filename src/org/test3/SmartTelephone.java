package org.test3;

public class SmartTelephone extends Telephone{

	@Override
	public void with() {
		System.out.println("Communication");
	}

	@Override
	public void lift() {
		System.out.println("Lift the phone");
	}

	@Override
	public void disconnected() {
		System.out.println("Phone is disconnected");
	}
	
	public static void main(String[] args) {
		SmartTelephone smart=new SmartTelephone();
		smart.with();
		smart.lift();
		smart.disconnected();
	}
	
	
	

}
