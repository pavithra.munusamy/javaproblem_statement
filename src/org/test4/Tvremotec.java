package org.test4;

public class Tvremotec implements Tvremote, SmartTvRemote {

	@Override
	public void bluetoothConnection() {
		System.out.println("bluetooth Pairing");
		
	}

	@Override
	public void batterysettings() {
		System.out.println("smart battery");
		
	}

	@Override
	public void tvLight() {
		System.out.println("signal light");
		
	}

	@Override
	public void tvBattery() {
		System.out.println("battery");
		
	}
	
	public static void main(String[] args) {
		Tvremotec tv=new Tvremotec();
		tv.batterysettings();
		tv.bluetoothConnection();
		tv.tvBattery();
		tv.tvLight();
	}

}
